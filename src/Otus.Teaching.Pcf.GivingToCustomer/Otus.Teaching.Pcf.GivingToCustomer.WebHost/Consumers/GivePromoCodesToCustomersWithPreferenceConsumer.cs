using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Services;
using Otus.Teaching.Pcf.Messages;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class GivePromoCodesToCustomersWithPreferenceConsumer : IConsumer<GivePromoCodeToCustomerCommand>
    {
        private readonly PromoCodesManager _promoCodesManager;

        public GivePromoCodesToCustomersWithPreferenceConsumer(PromoCodesManager promoCodesManager)
        {
            _promoCodesManager = promoCodesManager;
        }

        public Task Consume(ConsumeContext<GivePromoCodeToCustomerCommand> context)
        {
            return _promoCodesManager.GivePromoCodesToCustomersWithPreferenceAsync(context.Message);
        }
    }
}