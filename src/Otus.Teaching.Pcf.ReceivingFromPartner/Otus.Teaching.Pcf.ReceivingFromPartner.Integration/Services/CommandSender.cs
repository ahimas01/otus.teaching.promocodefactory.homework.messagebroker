using System;
using System.Threading.Tasks;
using MassTransit;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services
{
    public class CommandSender
    {
        private readonly IBus _bus;

        public CommandSender(IBus bus)
        {
            _bus = bus;
        }

        public async Task SendCommand(string queueUri, object command)
        {
            var endPoint = await _bus.GetSendEndpoint(new Uri(queueUri));
            await endPoint.Send(command);
        }
    }
}