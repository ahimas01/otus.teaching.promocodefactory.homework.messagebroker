using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationRabbitGateway
        : IAdministrationGateway
    {
        private readonly CommandSender _sender;
        private readonly AdministrationRabbitGatewayOption _option;

        public AdministrationRabbitGateway(CommandSender sender, IOptions<AdministrationRabbitGatewayOption> options)
        {
            _sender = sender;
            _option = options.Value;
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _sender.SendCommand(_option.AdministrationQueueUri,
                new UpdateEmployeeCommand() {Id = partnerManagerId});
        }
    }
}