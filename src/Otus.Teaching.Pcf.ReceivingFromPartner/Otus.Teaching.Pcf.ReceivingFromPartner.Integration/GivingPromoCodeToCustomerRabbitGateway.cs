using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Messages;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerRabbitGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly CommandSender _sender;
        private readonly GivingPromoCodeToCustomerOption _option;

        public GivingPromoCodeToCustomerRabbitGateway(CommandSender sender,
            IOptions<GivingPromoCodeToCustomerOption> options)
        {
            _sender = sender;
            _option = options.Value;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            await _sender.SendCommand(_option.GivingPromoCodeToCustomerQueueUri, new GivePromoCodeToCustomerCommand()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            });
        }
    }
}