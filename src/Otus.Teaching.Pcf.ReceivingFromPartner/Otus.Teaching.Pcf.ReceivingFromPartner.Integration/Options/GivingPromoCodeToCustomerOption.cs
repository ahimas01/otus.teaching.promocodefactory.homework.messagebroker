namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options
{
    public class GivingPromoCodeToCustomerOption
    {
        public string GivingPromoCodeToCustomerQueueUri { get; set; }
    }
}