namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Options
{
    public class AdministrationRabbitGatewayOption
    {
        public string AdministrationQueueUri { get; set; }
    }
}