using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using Otus.Teaching.Pcf.Administration.Core.Services;
using Otus.Teaching.Pcf.Messages;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class UpdateEmployeeConsumer : IConsumer<UpdateEmployeeCommand>
    {
        private readonly EmployeesManager _manager;

        public UpdateEmployeeConsumer(EmployeesManager manager)
        {
            _manager = manager;
        }

        public Task Consume(ConsumeContext<UpdateEmployeeCommand> context)
        {
            return _manager.UpdateAppliedPromocodesAsync(context.Message.Id);
        }
    }
}

namespace Otus.Teaching.Pcf.Messages
{
    public class UpdateEmployeeCommand
    {
        public Guid Id { get; set; }
    }
}