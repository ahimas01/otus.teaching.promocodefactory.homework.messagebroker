﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.MVC.Models;

namespace Otus.Teaching.Pcf.Administration.MVC.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly ILogger<EmployeesController> _logger;
        private readonly HttpClient _apiClient;

        public EmployeesController(ILogger<EmployeesController> logger, HttpClient apiClient)
        {
            _logger = logger;
            _apiClient = apiClient;
        }

        public async Task<IActionResult> Index()
        {
            var res = await _apiClient.GetAsync("http://localhost:8091/api/v1/Employees");
            if (!res.IsSuccessStatusCode) return BadRequest();
            var body = await res.Content.ReadAsStringAsync();
            var list = JsonSerializer.Deserialize<IEnumerable<EmployeeShortResponse>>(body);
            var vmRes = list.Select(s => new EmployeeShortVM(s));
            return View(vmRes);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public async Task<ActionResult> Details(Guid id)
        {
            var res = await _apiClient.GetAsync("http://localhost:8091/api/v1/Employees/" + id);
            if (!res.IsSuccessStatusCode) return BadRequest();
            var body = await res.Content.ReadAsStringAsync();
            var respone = JsonSerializer.Deserialize<EmployeeResponse>(body);
            var vmRes = new EmployeeVM(respone);
            return View(vmRes);
        }

        public async Task<IActionResult> UpdateAppliedPromocodesCount(EmployeeVM employeeVm)
        {
            var res = await _apiClient.PostAsync(
                $"http://localhost:8091/api/v1/Employees/{employeeVm.Id.ToString()}/appliedPromocodes", null);
            if (!res.IsSuccessStatusCode) return BadRequest();
            return Redirect("/Employees/Details/" + employeeVm.Id);
        }
    }
}