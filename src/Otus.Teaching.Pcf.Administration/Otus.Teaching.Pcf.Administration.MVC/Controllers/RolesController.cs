using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.MVC.Models;

namespace Otus.Teaching.Pcf.Administration.MVC.Controllers
{
    public class RolesController : Controller
    {
        private readonly ILogger<EmployeesController> _logger;
        private readonly HttpClient _apiClient;

        public RolesController(ILogger<EmployeesController> logger, HttpClient apiClient)
        {
            _logger = logger;
            _apiClient = apiClient;
        }

        // GET
        public async Task<IActionResult> Index()
        {
            var res = await _apiClient.GetAsync("http://localhost:8091/api/v1/Roles");
            if (!res.IsSuccessStatusCode) return BadRequest();
            var body = await res.Content.ReadAsStringAsync();
            var list = JsonSerializer.Deserialize<IEnumerable<RoleResponse>>(body);
            var vmRes = list.Select(s => new RoleVM(s));
            return View(vmRes);
        }
    }
}