using System;
using System.ComponentModel;

namespace Otus.Teaching.Pcf.Administration.MVC.Models
{
    public class RoleVM
    {
        public RoleVM()
        {
        }

        public RoleVM(RoleResponse employeeResponseRole)
        {
            Description = employeeResponseRole.Description;
            Id = employeeResponseRole.Id;
            Name = employeeResponseRole.Name;
        }

        [DisplayName("Ид")] public Guid Id { get; set; }
        [DisplayName("Описание")] public string Description { get; set; }
        [DisplayName("Название")] public string Name { get; set; }
    }
}