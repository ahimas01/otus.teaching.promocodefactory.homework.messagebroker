using System;
using System.Text.Json.Serialization;

namespace Otus.Teaching.Pcf.Administration.MVC.Models
{
    public class EmployeeResponse
    {
        [JsonPropertyName("id")] public Guid Id { get; set; }

        [JsonPropertyName("fullName")] public string FullName { get; set; }
        [JsonPropertyName("email")] public string Email { get; set; }
        [JsonPropertyName("role")] public RoleResponse Role { get; set; }

        [JsonPropertyName("appliedPromocodesCount")]
        public int AppliedPromocodesCount { get; set; }
    }
}