using System;
using System.ComponentModel;

namespace Otus.Teaching.Pcf.Administration.MVC.Models
{
    public class EmployeeVM
    {
        [DisplayName("Имя и фамилия")] public string FullName { get; set; }
        [DisplayName("Ид")] public Guid Id { get; set; }
        [DisplayName("Электронный адрес")] public string Email { get; set; }

        public RoleVM Role { get; set; }

        [DisplayName("Количество примененных промокодов")]
        public int AppliedPromocodesCount { get; set; }

        public EmployeeVM()
        {
        }

        public EmployeeVM(EmployeeResponse employeeResponse)
        {
            FullName = employeeResponse.FullName;
            Email = employeeResponse.Email;
            Id = employeeResponse.Id;
            AppliedPromocodesCount = employeeResponse.AppliedPromocodesCount;
            Role = new RoleVM(employeeResponse.Role);
        }
    }
}