using System;
using System.ComponentModel;

namespace Otus.Teaching.Pcf.Administration.MVC.Models
{
    public class EmployeeShortVM
    {
        [DisplayName("Имя и фамилия")] public string FullName { get; set; }
        [DisplayName("Ид")] public Guid Id { get; set; }
        [DisplayName("Электронный адрес")] public string Email { get; set; }

        public EmployeeShortVM(EmployeeShortResponse response)
        {
            FullName = response.FullName;
            Id = response.Id;
            Email = response.Email;
        }
    }
}