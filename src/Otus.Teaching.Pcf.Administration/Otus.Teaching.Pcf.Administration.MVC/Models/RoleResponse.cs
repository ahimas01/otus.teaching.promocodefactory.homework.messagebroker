using System;
using System.Text.Json.Serialization;

namespace Otus.Teaching.Pcf.Administration.MVC.Models
{
    public class RoleResponse
    {
        [JsonPropertyName("id")] public Guid Id { get; set; }
        [JsonPropertyName("name")] public string Name { get; set; }
        [JsonPropertyName("description")] public string Description { get; set; }
    }
}